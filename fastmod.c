/*
 * Copyright 2014 bsunau7
 *
 */

#include "cpuminer-config.h"
#include "miner.h"
#include "fastmod.h"

#include <arm_neon.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

// N is the array, C is the write back tail, I is the index into the array and R is the uint8x8_t
#define REMAP_NEW(N,C,I,R)  \
	uint32_t	_t1=0,_t2=0; \
	asm("vmov %[dst1],%[dst2],%[src]\n\t" : [dst1]"=r"(_t1), [dst2]"=r"(_t2) : [src]"w"((R)) ); \
	if(!(_t1 & 0x000000ff)) { (N)[(C)++] = (N)[(I)]; } \
	if(!(_t2 & 0x000000ff)) { (N)[(C)++] = (N)[(I)+2]; } \
	if(!(_t1 & 0x00ff0000)) { (N)[(C)++] = (N)[(I)+1]; } \
	if(!(_t2 & 0x00ff0000)) { (N)[(C)++] = (N)[(I)+3]; } \

#define REMAP(N,C,I,R) \
	if(!(R)[0]) { (N)[(C)++] = (N)[(I)]; } \
	if(!(R)[2]) { (N)[(C)++] = (N)[(I)+1]; } \
	if(!(R)[4]) { (N)[(C)++] = (N)[(I)+2]; } \
	if(!(R)[6]) { (N)[(C)++] = (N)[(I)+3]; } \

uint8_t  map[] __attribute__((aligned(32))) =
                 {1,0,0,0,0,1,1,0,   0,0,1,1,0,0,0,0,   0,0,0,0,0,0,0,0,   0,1,0,0,0,1,1,1,  // mod 23 map[0]
                  1,0,0,0,0,0,0,1,   0,0,1,0,0,0,0,1,   0,0,1,0,0,0,0,0,   0,1,0,0,0,0,0,1,  // mod 29 map[32]
                  1,0,0,0,0,0,0,1,   0,0,0,0,0,0,1,1,   0,0,0,0,0,0,1,0,   0,0,0,0,0,1,0,1,  // mod 31 map[64]
                  0,0,0,1,0,0,0,1,   0,0,0,0,0,1,0,0,   1,0,0,0,0,0,1,0,   0,1,1,0,0,1,0,1,  // mod 37 map[96]
                  0,0,1,0,0,0,0,1,   0,0,0,0,0,0,0,0,   0,1,0,0,0,1,0,0,   1,0,1,0,1,1,0,1,  // mod 41 map[128]
                  0,1,0,0,0,0,1,0,   0,0,0,1,0,0,0,0,   0,0,0,0,0,0,0,0,   0,1,0,0,0,0,1,1,  // mod 43 map[160]
                  0,0,0,0,0,1,0,0,   0,1,1,1,0,0,0,0,   0,0,0,0,0,0,0,0,   0,0,0,0,0,1,1,1,  // mod 47 map[192]
                  0,0,0,1,0,0,1,0,   0,0,0,1,0,0,1,1,   0,1,0,0,0,0,0,0,   0,0,0,0,0,0,0,1,  // mod 53 map[224]
                  0,0,0,0,0,0,0,1,   0,0,1,0,0,0,1,1,   0,0,0,0,0,0,1,0,   0,1,0,0,0,0,0,1,  // mod 59 map[256]
                  0,0,0,0,0,0,0,1,   0,0,0,0,0,0,1,1,   0,0,0,0,0,0,1,0,   0,0,0,0,0,1,0,1,  // mod 61 map[288]
                  0,0,0,1,1,1,0,0,   0,0,0,0,0,0,0,0,   0,1,1,0,0,0,0,0,   0,0,0,0,0,0,0,1,  // mod 67 map[320]
                  0,0,0,1,0,0,0,0,   0,1,0,0,1,0,0,0,   0,0,0,0,0,1,1,0,   0,0,0,0,0,0,0,1,  // mod 71 map[352]
                  0,0,0,1,0,0,0,0,   0,1,0,0,1,0,0,0,   0,0,0,0,0,0,1,0,   0,1,0,0,0,0,0,1,  // mod 73 map[384]
		  0,0,0,0,0,0,0,1,   0,0,0,0,0,0,0,0,   0,1,0,0,0,0,0,0,   1,0,1,0,0,1,0,1,  // mod 79 map[416]
		  0,0,1,0,0,0,0,1,   0,0,0,1,0,0,0,0,   0,0,0,0,0,1,0,0,   0,0,1,0,1,0,1,1}; // mod 83 map[448]

// mod29 code...  returns the numbe of candidates found.  Result array pre-populated.
// mod29 only prunes ~ 20% of candiates, so cheaper to do mod31 at the same time
__inline__ uint32_t  mod29n31(uint32_t *n, uint32_t off29, uint32_t off31) {
	uint32_t	i=0,c=0;

	uint8x8x4_t	m1 = vld4_u8(&map[32]);
	uint8x8x4_t	m2 = vld4_u8(&map[64]);
	uint32x4_t	o1 = vdupq_n_u32(off29);
	uint32x4_t	o2 = vdupq_n_u32(off31);
	uint32x4_t	s1 = vdupq_n_u32(0x08d3dcb0);
	uint32x4_t	s2 = vdupq_n_u32(0x08421084);

	uint32x4_t n1 = vld1q_u32(n);					// Load the 4 lanes pointer arrithmatic!

	for(i=4;n[i-4];i += 4) {
		uint32x4_t n2 = vaddq_u32(n1,o2);
		n1 = vaddq_u32(n1,o1);

		//applog(LOG_INFO," - mod29 %u %u %u %u",nn[0],nn[1],nn[2],nn[3]);
		// The mod29 code
		uint32x4_t e = vshrq_n_u32(n1,1);                               // do the first shift
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);			        // Mul & acc
		e = vshrq_n_u32(e,4);                                           // n>>5
		x1 = vaddq_u32(x1,e);                                           // and add
		e = vshrq_n_u32(e,1);                                           // n>>6
		x1 = vaddq_u32(x1,e);                                           // and add
		e = vshrq_n_u32(e,2);                                           // n>>8
		x1 = vaddq_u32(x1,e);                                           // and add
		x1 = vshrq_n_u32(x1,27);                                        // and shift to get the answer.

		n1 = vld1q_u32(n+i);						// pre-load for the next go around

		// The mod31 code
		e = vshrq_n_u32(n2,3);
		uint32x4_t x2 = vmlaq_u32(e,n2,s2);			        // Mul & acc
		e = vshrq_n_u32(e,5);                                           // n>>8
		x2 = vaddq_u32(x2,e);                                           // and add
		x2 = vshrq_n_u32(x2,27);                                        // and shift to get the answer.

		// The maps
		// mod27
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       ret1 = vtbl4_u8(m1,r1);

		// mod31
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		uint8x8_t       ret2 = vtbl4_u8(m2,r1);

		// As we only care about true and false multiple ret1 and ret2
		r1 = vorr_u8(ret1,ret2);

		REMAP(n,c,i,r1);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	//applog(LOG_INFO,"accepted %u returned %u",i,c);
	return(c);
}

__inline__ uint32_t  mod37n41(uint32_t *n, uint32_t off37, uint32_t off41) {
	uint32_t	i=0,c=0;

	uint8x8x4_t	m1 = vld4_u8(&map[96]);
	uint8x8x4_t	m2 = vld4_u8(&map[128]);
	uint32x4_t	off1 = vdupq_n_u32(off37);
	uint32x4_t	off2 = vdupq_n_u32(off41);
	uint32x4_t	s1 = vdupq_n_u32(0x06EB3E45);
	uint32x4_t	s2 = vdupq_n_u32(0x063E7064);


	for(i=4;n[i-4];i+=4) {
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);
                n1 = vaddq_u32(n1,off1);

		// 37
		//uint32x4_t n1 = vmovq_n_u32(n);
		uint32x4_t e = vshrq_n_u32(n1,3);                               // do the first shift
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);         // Mul & acc
		e = vshrq_n_u32(e,1);                                           // n>>5
		x1 = vaddq_u32(x1,e);                                             // and add
		x1 = vshrq_n_u32(x1,26);                                          // and shift to get the answer.

		// 41
		//uint32x4_t n2 = vmovq_n_u32(n);
		e = vshrq_n_u32(n2,4);
		uint32x4_t x2 = vmulq_u32(n2,s2);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,1);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,2);
		x2 = vsubq_u32(x2,e);
		x2 = vshrq_n_u32(x2,26);

		// 37
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);                           // Bring in the mod37 result

		// 41
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}

__inline__ uint32_t  mod43n47(uint32_t *n, uint32_t off43, uint32_t off47) {
	uint32_t	i=0,c=0;

	uint8x8x4_t     m1 = vld4_u8(&map[160]);
	uint8x8x4_t	m2 = vld4_u8(&map[192]);
	uint32x4_t	off1 = vdupq_n_u32(off43);
	uint32x4_t	off2 = vdupq_n_u32(off47);
	uint32x4_t	s1 = vdupq_n_u32(0x05F417D0);
	uint32x4_t	s2 = vdupq_n_u32(0x0572620B);

	for(i=0;n[i];i+=4) {
		//__builtin_prefetch(n+512);
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);

                n1 = vaddq_u32(n1,off1);

		// 43
		uint32x4_t e = vshrq_n_u32(n1,1);
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);
		e = vshrq_n_u32(e,2);
		x1 = vsubq_u32(x1,e);
		x1 = vshrq_n_u32(x1,26);

		// 47
		e = vshrq_n_u32(n2,3);
		uint32x4_t x2 = vmulq_u32(n2,s2);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,3);
		x2 = vaddq_u32(x2,e);
		x2 = vshrq_n_u32(x2,26);

		// 43
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);

		// 47
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}

__inline__ uint32_t  mod53n59(uint32_t *n, uint32_t off53, uint32_t off59) {
	uint32_t	i=0,c=0;

	uint8x8x4_t     m1 = vld4_u8(&map[224]);
	uint8x8x4_t	m2 = vld4_u8(&map[256]);
	uint32x4_t	off1 = vdupq_n_u32(off53);
	uint32x4_t	off2 = vdupq_n_u32(off59);
	uint32x4_t	s1 = vdupq_n_u32(0x04D4873f);
	uint32x4_t	s2 = vdupq_n_u32(0x0456C798);

	for(i=0;n[i];i+=4) {
		//__builtin_prefetch(n+512);
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);

                n1 = vaddq_u32(n1,off1);

		// 53
		uint32x4_t e = vshrq_n_u32(n1,4);
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);
		e = vshrq_n_u32(n1,2);
		x1 = vsubq_u32(x1,e);
		e = vshrq_n_u32(e,4);
		x1 = vsubq_u32(x1,e);
		e = vshrq_n_u32(e,2);
		x1 = vsubq_u32(x1,e);
		x1 = vshrq_n_u32(x1,26);

		// 59
		e = vshrq_n_u32(n2,10);
		uint32x4_t x2 = vmlaq_u32(e,n2,s2);
		e = vshrq_n_u32(n2,3);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,4);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,1);
		x2 = vsubq_u32(x2,e);
		x2 = vshrq_n_u32(x2,26);

		// 53
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);

		// 59
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}

__inline__ uint32_t  mod61n67(uint32_t *n, uint32_t off61, uint32_t off67) {
	uint32_t	i=0,c=0;

	uint8x8x4_t     m1 = vld4_u8(&map[288]);
	uint8x8x4_t	m2 = vld4_u8(&map[320]);
	uint32x4_t	off1 = vdupq_n_u32(off61);
	uint32x4_t	off2 = vdupq_n_u32(off67);
	uint32x4_t	s1 = vdupq_n_u32(0x04325c54);
	uint32x4_t	s2 = vdupq_n_u32(0x03d22635);

	for(i=0;n[i];i+=4) {
		//__builtin_prefetch(n+512);
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);

                n1 = vaddq_u32(n1,off1);

		// 61
		uint32x4_t e = vshrq_n_u32(n1,10);
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);
		e = vshrq_n_u32(n1,4);
		x1 = vsubq_u32(x1,e);
		e = vshrq_n_u32(e,4);
		x1 = vsubq_u32(x1,e);
		x1 = vshrq_n_u32(x1,26);

		//  67
		e = vshrq_n_u32(n2,1);
		uint32x4_t x2 = vmlaq_u32(e,n2,s2);
		e = vshrq_n_u32(n2,7);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,5);
		x2 = vaddq_u32(x2,e);
		x2 = vshrq_n_u32(x2,25);

		// 61 
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(32));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);

		// 67
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(96));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}

__inline__ uint32_t  mod71n73(uint32_t *n, uint32_t off71, uint32_t off73) {
	uint32_t	i=0,c=0;

	uint8x8x4_t     m1 = vld4_u8(&map[352]);
	uint8x8x4_t	m2 = vld4_u8(&map[384]);
	uint32x4_t	off1 = vdupq_n_u32(off71);
	uint32x4_t	off2 = vdupq_n_u32(off73);
	uint32x4_t	s1 = vdupq_n_u32(0x039b0ad1);
	uint32x4_t	s2 = vdupq_n_u32(0x0381c0e0);

	for(i=0;n[i];i+=4) {
		//__builtin_prefetch(n+512);
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);

                n1 = vaddq_u32(n1,off1);

		// 71
		uint32x4_t e = vshrq_n_u32(n1,3);
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);
		e = vshrq_n_u32(n1,9);
		x1 = vaddq_u32(x1,e);
		x1 = vshrq_n_u32(x1,25);

		//  73
		e = vshrq_n_u32(n2,1);
		uint32x4_t x2 = vmlaq_u32(e,n2,s2);
		e = vshrq_n_u32(n2,4);
		x2 = vsubq_u32(x2,e);
		x2 = vshrq_n_u32(x2,25);

		// 71 
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(96));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);

		// 73
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(96));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}

__inline__ uint32_t  mod79n83(uint32_t *n, uint32_t off79, uint32_t off83) {
	uint32_t	i=0,c=0;

	uint8x8x4_t     m1 = vld4_u8(&map[416]);
	uint8x8x4_t	m2 = vld4_u8(&map[448]);
	uint32x4_t	off1 = vdupq_n_u32(off79);
	uint32x4_t	off2 = vdupq_n_u32(off83);
	uint32x4_t	s1 = vdupq_n_u32(0x033d91d2);
	uint32x4_t	s2 = vdupq_n_u32(0x03159722);

	for(i=0;n[i];i+=4) {
		//__builtin_prefetch(n+512);
		uint32x4_t n1 = vld1q_u32(n+i);
		uint32x4_t n2 = vaddq_u32(n1,off2);

                n1 = vaddq_u32(n1,off1);

		// 79
		uint32x4_t e = vshrq_n_u32(n1,1);
		uint32x4_t x1 = vmlaq_u32(e,n1,s1);
		e = vshrq_n_u32(n1,3);
		x1 = vaddq_u32(x1,e);
		e = vshrq_n_u32(n1,7);
		x1 = vaddq_u32(x1,e);
		x1 = vshrq_n_u32(x1,25);

		//  83
		e = vshrq_n_u32(n2,4);
		uint32x4_t x2 = vmlaq_u32(e,n2,s2);
		e = vshrq_n_u32(n2,3);
		x2 = vsubq_u32(x2,e);
		e = vshrq_n_u32(e,4);
		x2 = vsubq_u32(x2,e);
		x2 = vshrq_n_u32(x2,25);

		// 79 
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
		uint8x8_t       r2 = vceq_u8(r1,vdup_n_u8(0));  // Catch the '0' before we lose it
		r1 = vsub_u8(r1,vdup_n_u8(96));                 // Now scale down to fir
		uint8x8_t       ret = vtbl4_u8(m1,r1);
		ret = vorr_u8(ret,r2);

		// 83
		// Because we are above 32 can't use the table map, so hard code in a set of tests
		r1 = vreinterpret_u8_u16(vmovn_u32(x2));
		r2 = vceq_u8(r1,vdup_n_u8(0));                  // Catch the '0' before we lose it
		r2 = vorr_u8(ret,r2);                           // Bring in the mod37 result
		r1 = vsub_u8(r1,vdup_n_u8(96));                 // Now scale down to fir
		ret = vtbl4_u8(m2,r1);

		ret = vorr_u8(ret,r2);                          // Bring the '0' case back in.

		REMAP(n,c,i,ret);
	}
	// Now we zero out the tail.  4 times to allow for later vec code to test correctly
	n[c] = 0L;
	n[c+1] = 0L;
	n[c+2] = 0L;
	n[c+3] = 0L;

	return(c);
}
