/*
 * Copyright 2014 bsunau7
 *
 */

#ifndef __FASTMOD_H__
#define __FASTMOD_H__

#include "fastmod.h"

#include <arm_neon.h>
#include <stdint.h>
#include <inttypes.h>

extern uint8_t	map[];

extern uint32_t	mod29n31(uint32_t *n, uint32_t off29, uint32_t off31);
extern uint32_t	mod37n41(uint32_t *n, uint32_t off37, uint32_t off41);
extern uint32_t	mod43n47(uint32_t *n, uint32_t off43, uint32_t off47);
extern uint32_t	mod53n59(uint32_t *n, uint32_t off53, uint32_t off59);
extern uint32_t	mod61n67(uint32_t *n, uint32_t off61, uint32_t off67);
extern uint32_t	mod71n73(uint32_t *n, uint32_t off71, uint32_t off73);
extern uint32_t	mod79n83(uint32_t *n, uint32_t off79, uint32_t off83);

extern void	fast_recip(uint32_t **vec,int **poff,int curr_s,uint32_t p);

#endif /* __FASTMOD_H__ */
