/* mpz_millerrabin(n,reps) -- An implementation of the probabilistic primality
   test found in Knuth's Seminumerical Algorithms book.  If the function
   mpz_millerrabin() returns 0 then n is not prime.  If it returns 1, then n is
   'probably' prime.  The probability of a false positive is (1/4)**reps, where
   reps is the number of internal passes of the probabilistic algorithm.  Knuth
   indicates that 25 passes are reasonable.

   THE FUNCTIONS IN THIS FILE ARE FOR INTERNAL USE ONLY.  THEY'RE ALMOST
   CERTAIN TO BE SUBJECT TO INCOMPATIBLE CHANGES OR DISAPPEAR COMPLETELY IN
   FUTURE GNU MP RELEASES.

Copyright 1991, 1993, 1994, 1996-2002, 2005 Free Software Foundation, Inc.

Contributed by John Amanatides.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp.h"
#include "miner.h"

#define SIZ(x) ((x)->_mp_size)
#define PTR(x) ((x)->_mp_d)

static int my_millerrabin2 (mpz_srcptr, mpz_srcptr, mpz_ptr, mpz_ptr, mpz_srcptr, unsigned long int);

// Initialize the stuff needed for the MR test so we don't have to do it mulitple times

int init_mymr(struct mr_struct *mr,mpz_srcptr n) { 
	mpz_init2(mr->nm1, SIZ (n) + 1);
	mpz_init2(mr->x, SIZ (n) + 1);
	mpz_init2(mr->y, 2 * SIZ (n)); /* mpz_powm_ui needs excessive memory!!! */
	mpz_init2(mr->q, SIZ (n));
	mpz_init2(mr->nm3, SIZ (n) + 1);

	mpz_init(mr->fmt);
	//mpz_set_ui (mr->x210, 210L);
	//mpz_set_ui (mr->fmt, 304250263527210L); //p41
	//mpz_set_ui (mr->fmt, 223092870L); //p23
	mpz_set_ui (mr->fmt, 2L); //p1

	gmp_randinit_default (mr->rstate);
	return(0);
}

int free_mymr(struct mr_struct *mr) {
	mpz_clears(mr->nm1,mr->x,mr->y,mr->q,mr->nm3,mr->fmt,0L);
	gmp_randclear (mr->rstate);
	return(0);
}

int my_fermat(mpz_srcptr n, struct mr_struct *mr) {
	mpz_sub_ui (mr->nm1, n, 1L);

	/* Perform a Fermat test.  */
	mpz_powm (mr->y, mr->fmt, mr->nm1, n);
	if (mpz_cmp_ui (mr->y, 1L) != 0) {
		return 0;
	}
	return 1;
}

int my_millerrabin(mpz_srcptr n, int reps, struct mr_struct *mr) {
	int r,is_prime;
	unsigned long int k;

	mpz_sub_ui (mr->nm1, n, 1L);

	/* Perform a Fermat test.  */
	mpz_powm (mr->y, mr->fmt, mr->nm1, n);
	if (mpz_cmp_ui (mr->y, 1L) != 0) {
		return 0;
	}

	/* Find q and k, where q is odd and n = 1 + 2**k * q.  */
	k = mpz_scan1 (mr->nm1, 0L);
	mpz_tdiv_q_2exp (mr->q, mr->nm1, k);

	/* n-3 */
	mpz_sub_ui (mr->nm3, n, 3L);

	is_prime = 1;
	for (r = 0; r < reps && is_prime; r++) {
		/* 2 to n-2 inclusive, don't want 1, 0 or -1 */
		mpz_urandomm (mr->x, mr->rstate, mr->nm3);
		mpz_add_ui (mr->x, mr->x, 2L);

		is_prime = my_millerrabin2 (n, mr->nm1, mr->x, mr->y, mr->q, k);
	}

	return is_prime;
}

// Inline as we have more that 4 parameters, so avoiding function call by inlining seems like
// a good idea for the minimal gain it'll get us.

static __inline__ int
my_millerrabin2 (mpz_srcptr n, mpz_srcptr nm1, mpz_ptr x, mpz_ptr y, mpz_srcptr q, unsigned long int k) {
	unsigned long int i;

	mpz_powm (y, x, q, n);

	if (mpz_cmp_ui (y, 1L) == 0 || mpz_cmp (y, nm1) == 0)
		return 1;

	for (i = 1; i < k; i++) {
		mpz_powm_ui (y, y, 2L, n);
		if (mpz_cmp (y, nm1) == 0)
			return 1;
		if (mpz_cmp_ui (y, 1L) == 0)
			return 0;
	}
	return 0;
}
