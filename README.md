RIECOIN ARMv7 Miner
===================

Welcome to the ARMv7 solo miner for RieCoin (http://riecoin.org).  This is an
ARM based port of the original cpuminer (https://github.com/gatra/cpuminer-rminerd)
which has been heavily optimized for NEON based CPUs.

This is being released as I've done little in the last month and it seems only
fair to allow others to help me (help may just mean encouragement!).  I always
accept donations (see below).

NOTE:  This is not the cleanest code I have ever written, but every time I try
to add abstraction to make it cleaner I reduce performance, so in a manner
I've given up on abstraction which is why it is a little monolithic.

To Build:
=========

This has the same dependencies as the original cpuminer.

$ ./autogen

$ ./configure CFLAGS="- -marm -O3 -fomit-frame-pointer -march=armv7-a -mfpu=neon -mtune=cortex-a15"

$ make -j4

OPTIONALLY.  Add "-static -L/usr/local/lib" with statically compiled libraries
(in /usr/local/lib) to allow for a static binary.

To Run:

I use the following command line, the -m option is mondified/unique:

./rminerd -o <host>:28332 -O <user>:<password> -m 15750000 -t 4

-m is the maximum prime to use in the sieve.  I tend to set this such that the
Fermat tests and the sieve run in about the same time.

Binaries:
=========

These can be fetched from https://mega.co.nz/#F!6Y8ywRoZ!5ogMJEc2Teq5ckt9WClNpg
they might be a little out of date, but I.ll try to update is any miner with a
nice speed improvement is available.

Donations:
==========

RieCoin has always been a hobby, something to keep my mind amused and
stimulated.  Having said that if you feel like donating some coins please do:

RNRsT9Ft7dyAwbY2RcBwcrxVAB979B8D7S

I would LOVE to buy a ARMv8 development board ;-)
