/*
 * Copyright 2011 ArtForz
 * Copyright 2011-2013 pooler
 * Copyright 2013 gatra
 * Copyright 2014 bsunau7
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.  See COPYING for more details.
 */

#include "cpuminer-config.h"
#include "miner.h"
#include "fastmod.h"

#include <arm_neon.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <gmp.h>
#include <inttypes.h>
#include <sys/time.h>

int		tp0=0,tp1=0,tp2=0,tp3=0,tp4=0,tp5=0,tp6=0;
uint32_t	*ps,*precip,*mod32bit_a,*mod32bit_b;
uint32_t	pcount=0;
uint32_t	*pn23;
int		global_s=8;

#define PN23			223092870
#define PN23_SZ			85085
#define VEC_SZ			(4*256*1024)	// 3.99 * 256k but round up to give us a buffer
#define MAX_S			16
#define TARGET_T		30		// target block time, adjust 'global_s' to match
#define OFFSET_TWEAK		0

#define CHECK 0

// MPZ_FAST_ADD/SUB assumes that no limb overflow will happen, if it will then don't use.
#define MPZ_FAST_ADD(x,y)	(x->_mp_d[0] = x->_mp_d[0] + y)
#define MPZ_FAST_SUB(x,y)	(x->_mp_d[0] = x->_mp_d[0] - y)

// Some alignment to speed NEON up abit (I hope) (from GNU radio, if released break out into
// seperate file malloc16.h and malloc16.c
// As we start looping on prime number 9 we need to tweek the alignment such that the
// 9th element is 16 byts aligned.

void *malloc16 (int size) {
	void *p;
	void **p1;
	if((p = malloc(size+31+OFFSET_TWEAK)) == NULL)
		return NULL;
	/* Round up to next 16-byte boundary */
	p1 = (void **)(((long)p + 31) & (~15) + OFFSET_TWEAK);
	/* Stash actual start of block just before ptr we return */
	p1[-1] = p;
	/* Return 16-byte aligned address */
	return (void *)p1;
}

void free16(void *p) {
	if(p != NULL){
		/* Retrieve pointer to actual start of block and free it */
		free(((void **)p)[-1]);
	}
}

// This gets called once from cpu-miner.c
int initRiecoin(int plimit) {
	// generate primes
	uint32_t	*ptmp = malloc(sizeof(uint32_t)*plimit);

	for (uint32_t i=2;i<plimit;i++)
		ptmp[i]=1;

	for (uint32_t i=2;i<plimit;i++)
		if (ptmp[i])
			for (uint32_t j=i;i*j<plimit;j++)
				ptmp[i*j]=0;

	// Now to count and map
	for (uint32_t i=2;i<plimit;i++)
                if (ptmp[i])
			pcount++;

	ps = malloc16(sizeof(uint32_t)*pcount);
	pcount = 0;
	for(uint32_t i=2;i<plimit;i++)
                if (ptmp[i]) {
			ps[pcount++] = i;
		}

	free(ptmp);

	applog(LOG_INFO, "%u primes generated.",pcount);

	// Need to calculate reciprocals, delta (for the fast modulus)
	//mod32bit is needed to calculcate poff 1..3 using pof[0] as the base, it is quicker than gmp calls
	precip = malloc16(sizeof(uint32_t)*pcount);
	mod32bit_a = malloc16(sizeof(uint32_t)*pcount);
	mod32bit_b = malloc16(sizeof(uint32_t)*pcount);
	for(uint32_t i=2;i<pcount;i++) {
		precip[i] = (uint32_t)((1ULL<<32)/ps[i]);
		// We need to mod32 ar 19*PN23 or 4238764530
		mod32bit_a[i] = (uint32_t)((4238764530ULL)%ps[i]);
		// We need to mod32 ar 20*PN23 or 4461857400
		mod32bit_b[i] = (uint32_t)((4461857400ULL)%ps[i]);
	}

	applog(LOG_INFO, "Invariant data precomputed");

	// Try to load the pn23 file
	int		pn23_fd;
	struct	stat	pn23_stat;

	pn23_fd=open("pn23.bin",O_RDWR|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	fstat(pn23_fd,&pn23_stat);
	
	pn23 = malloc16(sizeof(uint32_t)*PN23_SZ);

	if((pn23_fd < 0) || (pn23_stat.st_size == 0)) {
		// Now built up pn23
		uint32_t c = 0;
		
		// As the primes are less than 32 we can't use my mapping or modulo arithmatic tricks
		for(uint32_t i=0;i<PN23;i++) {
			for(uint32_t j=0;j<9;j++) {
				if((i%ps[j]) == 0) goto end;
				if(((i+4)%ps[j]) == 0) goto end;
				if(((i+6)%ps[j]) == 0) goto end;
				if(((i+10)%ps[j]) == 0) goto end;
				if(((i+12)%ps[j]) == 0) goto end;
				if(((i+16)%ps[j]) == 0) goto end;
			}
			pn23[c++] = i;
	end:            ;;
		}
		applog(LOG_INFO, "pn23 sieve generated, %u entries",c);
		c = write(pn23_fd,pn23,sizeof(uint32_t)*c);
	} else {
		// Could mmap but as we don't know if we will malloc or mmap
		// better to just read the whole thing in
		if(pn23_stat.st_size != (sizeof(uint32_t)*PN23_SZ)) {
			applog(LOG_INFO, "pn23.bin is to small, delete it and start again");
			exit(1);
		}
		// Open and read into pn23
		uint32_t c = 0;
		c = read(pn23_fd,pn23,(sizeof(uint32_t)*PN23_SZ));
		if(c != (sizeof(uint32_t)*PN23_SZ)) {
			applog(LOG_INFO, "pn23.bin was not read correctly, expected:%u bytes got %u",(sizeof(uint32_t)*PN23_SZ),c);
			exit(1);
		}
		applog(LOG_INFO, "pn23 sieve read from file, %u entries",c/sizeof(uint32_t));
	}
	close(pn23_fd);

	return 0;
}

// From the origional riecoin

extern void sha256d_80_swap(uint32_t *hash, const uint32_t *data);

const int zeroesBeforeHashInPrime = 8;

void SetCompact(mpz_t p, uint32_t nCompact)
{
        unsigned int nSize = nCompact >> 24;
        unsigned int nWord = nCompact & 0x007fffff;

        if (nSize <= 3) {
            nWord >>= 8*(3-nSize);
            mpz_init_set_ui (p, nWord);
        } else {
            mpz_init_set_ui (p, nWord);
            mpz_mul_2exp(p, p, 8*(nSize-3));
        }
}

#define HASH_LEN_IN_BITS 256

unsigned int getTrailingZeros( uint32_t compactBits ) {
	mpz_t nBits;
	SetCompact( nBits, compactBits );
	unsigned int trailingZeros = mpz_get_ui (nBits) - 1 - zeroesBeforeHashInPrime - HASH_LEN_IN_BITS;
	mpz_clear(nBits);
	return trailingZeros;
}

unsigned int generatePrimeBase( mpz_t bnTarget, uint32_t *hash, uint32_t compactBits ) {
	int i;

	mpz_set_ui(bnTarget, 1);
	mpz_mul_2exp(bnTarget,bnTarget,zeroesBeforeHashInPrime);

	for ( i = 0; i < HASH_LEN_IN_BITS; i++ ) {
		mpz_mul_2exp(bnTarget,bnTarget,1);
		mpz_add_ui(bnTarget,bnTarget,(hash[i/32] & 1));
		hash[i/32] >>= 1;
	}

	mpz_t nBits;
	SetCompact( nBits, compactBits );
	unsigned int trailingZeros = mpz_get_ui (nBits) - 1 - zeroesBeforeHashInPrime - HASH_LEN_IN_BITS;
	mpz_mul_2exp(bnTarget,bnTarget,trailingZeros);
	mpz_clear(nBits);
	return trailingZeros;
}

double riecoin_time_to_block( double hashrate, uint32_t compactBits, int primes ) {
	mpz_t nBits;
	double f;
	static const double l2 = 0.69314718056; // ln(2)

	SetCompact( nBits, compactBits );
	f = mpz_get_ui (nBits);
	mpz_clear(nBits);
	
	f = pow( f * l2, primes ) / hashrate;
	return f * 0.05780811; // reciprocal of Hardy-Littlewood constant H6
}

#define MR_TESTS 12

int scanhash_riecoin(int thr_id, uint32_t *pdata, const int primes,
	uint64_t max_nonce, uint64_t *hashes_done, uint32_t *pSieve)
{
	uint32_t	hash[8] __attribute__((aligned(32)));
	//uint64_t	n = *(uint64_t *)(pdata + RIECOIN_DATA_NONCE);
	const uint64_t  first_nonce = *(uint64_t *)(pdata + RIECOIN_DATA_NONCE);
	uint32_t	j;
	struct timeval	tv_start, tv_offset, tv_sieve1, tv_sieve2, tv_end, diff, diff1, diff2, diff3, diff4;
	// a group of 4 vectors to to hold the 2^32+0, 2^32+1, 2^32+s ... vectors
	mpz_t		base,b[MAX_S];
	int32_t		*poff[MAX_S];
	uint32_t	*vec[MAX_S];
	uint32_t	r[MAX_S];
	uint64_t	n[MAX_S];
	int		curr_s = global_s;

	//applog(LOG_INFO,"thr:%u max_nonce:0x%" PRIx64 " first_nonce:0x%" PRIx64 " range:0x%" PRIx64, \
	       thr_id,(uint64_t)max_nonce,(uint64_t)first_nonce,(uint64_t)(max_nonce-first_nonce));

	// Start
	gettimeofday(&tv_start, NULL);

	mpz_init(base);
	for(int s=0;s<curr_s;s++) {
		mpz_init(b[s]);
	}
	
        uint64_t aux = *(uint64_t *)&pdata[RIECOIN_DATA_NONCE];
        *(uint32_t *)&pdata[RIECOIN_DATA_NONCE] = 0x80000000UL;
        *(((uint32_t *)&pdata[RIECOIN_DATA_NONCE]) + 1) = 0;
	sha256d_80_swap(hash, pdata);
	*(uint64_t *)&pdata[RIECOIN_DATA_NONCE] = aux;

	generatePrimeBase( base, hash, swab32(pdata[RIECOIN_DATA_DIFF]) );

	// Do we realy need to keep track of n[0..3]?
	n[0] = first_nonce;
	for(int s=1;s<curr_s;s++) {
		n[s] = n[s-1] + (1ULL<<32);
	}
	//applog(LOG_INFO,"thr %u: n[0]:%" PRIx64 " n[1]:%" PRIx64 " n[2]:%" PRIx64 " n[3]:%" PRIx64,thr_id,n[0],n[1],n[2],n[3]);

	mpz_add_ui(b[0],base, (0xffffffff & first_nonce));		// Set low 32 bits
	b[0]->_mp_d[1] += (first_nonce>>32);				// set high 32 bits
	for(int s=1;s<curr_s;s++) {
		mpz_set(b[s],b[s-1]);
		b[s]->_mp_d[1] += 1;
	}

	//gmp_printf("b0 %#Zx\nb1 %#Zx\nb2 %#Zx\nb3 %#Zx\n",b[0],b[1],b[2],b[3]);

	// Align the loop with the primorial by making n aligned
	for(int s=0;s<curr_s;s++) {
		r[s] = mpz_fdiv_ui(b[s], PN23);
		n[s] += PN23-r[s];
		mpz_add_ui(b[s],b[s],PN23-r[s]);
	}
	//applog(LOG_INFO,"thr %u: n[0]:%" PRIx64 " n[1]:%" PRIx64 " n[2]:%" PRIx64 " n[3]:%" PRIx64,thr_id,n[0],n[1],n[2],n[3]);

	// Ok, we need to do an offset from 'b' for the dynamic skip prime code
	for(int s=0;s<curr_s;s++) {
		poff[s] = malloc16(sizeof(uint32_t)*pcount);
	}

	// Start at 9 as well, we don't care about anything else...
	for(int i = 9;i<pcount;i++) {
		poff[0][i] = mpz_fdiv_ui(b[0],ps[i]);
	}

	// Fast calcualte poff 1..3 the increment is either 19 or 20 pn23 depending on
	// rounding etc.  So we detect and use the correct modular offsets when adding
	for(int s=1;s<curr_s;s++) {
		uint32_t	*mod32bit;
		mpz_t	t; mpz_init(t);
		mpz_sub(t,b[s],b[s-1]);

		if(mpz_get_ui(t) == 166890104) {
			mod32bit = mod32bit_b;
		} else {
			mod32bit = mod32bit_a;
		}

		for(int i = 9;i<pcount;i++) {
			uint32_t	tmp=0;
			poff[s][i] = poff[s-1][i] + mod32bit[i];
			if(poff[s][i] >= ps[i]) {
				poff[s][i] -= ps[i];
			}

			// test
#if CHECK
			if(poff[s][i] != mpz_fdiv_ui(b[s],ps[i])) {
				applog(LOG_INFO,"Fast mod error s:%u i:%u b delta:%u",s,i,mpz_get_ui(t));
			}
#endif
		}
		mpz_clear(t);
	}
	
	gettimeofday(&tv_offset, NULL);
	// A vector to hold the candidates
	for(int i=0;i<curr_s;i++) {
		vec[i] = malloc16(sizeof(uint32_t*)*VEC_SZ);
		memset(vec[i],0x00,16*sizeof(uint32_t*));	// Just zero the first few
	}

	uint8x8x4_t     map1 = vld4_u8(&map[32]);
	uint8x8x4_t     map2 = vld4_u8(&map[64]);
	uint32x4_t	s1 = vdupq_n_u32(0x08d3dcb0);
	uint32x4_t	s2 = vdupq_n_u32(0x08421084);

	for(int s=0;s<curr_s;s++) {
		uint32_t	cand=0;
		// Offsets are on a per stream basis
		uint32x4_t	o1 = vdupq_n_u32(poff[s][9]);
		uint32x4_t	o2 = vdupq_n_u32(poff[s][10]);

		for(uint32_t i=0;i<((0xfffffff0)/PN23);i++) {
			for(uint32_t j=0;j<(PN23_SZ-3);j+=4) {	// We will have 1 candidate at the end, not realy worth recovering
				uint32x4_t n1 = vld1q_u32(pn23+j);
				uint32x4_t n2 = vaddq_u32(n1,o2);
				n1 = vaddq_u32(n1,o1);

				// The mod29 code
				// Using n1 for the last shift to help with pipeline issues
				uint32x4_t e = vshrq_n_u32(n1,1);                               // do the first shift
				uint32x4_t x1 = vmlaq_u32(e,n1,s1);       			 // Mul & acc
				e = vshrq_n_u32(e,4);                                           // n>>5
				x1 = vaddq_u32(x1,e);                                           // and add
				e = vshrq_n_u32(e,1);                                           // n>>6
				x1 = vaddq_u32(x1,e);                                           // and add
				e = vshrq_n_u32(e,2);                                           // n>>8
				x1 = vaddq_u32(x1,e);                                           // and add
				x1 = vshrq_n_u32(x1,27);                                        // and shift to get the answer.

				// The mod31 code
				e = vshrq_n_u32(n2,3);
				uint32x4_t x2 = vmlaq_u32(e,n2,s2);			        // Mul & acc
				e = vshrq_n_u32(e,5);                                           // n>>8
				x2 = vaddq_u32(x2,e);                                           // and add
				x2 = vshrq_n_u32(x2,27);                                        // and shift to get the answer.

				// The maps
				// mod29
				uint8x8_t       r1 = vreinterpret_u8_u16(vmovn_u32(x1));
				uint8x8_t       ret1 = vtbl4_u8(map1,r1);

				// mod31
				r1 = vreinterpret_u8_u16(vmovn_u32(x2));
				uint8x8_t       ret2 = vtbl4_u8(map2,r1);

				// As we only care about true and false multiple ret1 and ret2
				r1 = vorr_u8(ret1,ret2);

				// Now push into vec
				if(!r1[0]) { vec[s][cand++] = pn23[j] + i*PN23; }
				if(!r1[2]) { vec[s][cand++] = pn23[j+1] + i*PN23; }
				if(!r1[4]) { vec[s][cand++] = pn23[j+2] + i*PN23; }
				if(!r1[6]) { vec[s][cand++] = pn23[j+3] + i*PN23; }

				if(cand >= VEC_SZ) {
					// overflow on the vexctor
					applog(LOG_INFO,"vector overflow i:%u j:%u cand:%u, truncating candidates",i,j,cand);
					cand--;
					goto out1;		
				}
			}
			// we cheat a little and accumulate PN23 into the offset.
			o1 = vaddq_u32(o1,vdupq_n_u32(PN23));
			o2 = vaddq_u32(o2,vdupq_n_u32(PN23));
		}
		// Ok we have exited for what ever reason.
		vec[s][cand++] = 0L;
		vec[s][cand++] = 0L;
		vec[s][cand++] = 0L;
	out1:
		vec[s][cand++] = 0L;
	}

	// custom code for the first few numbers.  Do them in pairs to vector better
	gettimeofday(&tv_sieve1, NULL);
	
	int c1,c2,c3,c4,c5,c6;
	c1=c2=c3=c4=c5=c6=0;
	for(int s=0;s<curr_s;s++) {
		// Moved 29n31 up to the pn23 generator loop
		//c1 += mod29n31(vec[s],poff[s][9] ,poff[s][10]);
		c1 += mod37n41(vec[s],poff[s][11],poff[s][12]);
		c2 += mod43n47(vec[s],poff[s][13],poff[s][14]);
		c3 += mod53n59(vec[s],poff[s][15],poff[s][16]);
		c4 += mod61n67(vec[s],poff[s][17],poff[s][18]);
//		c5 += mod71n73(vec[s],poff[s][19],poff[s][20]);
//		c6 += mod79n83(vec[s],poff[s][21],poff[s][22]);
	}

#if CHECK
	applog(LOG_INFO,"%u streams counts %u %u %u %u %u %u",curr_s,c1,c2,c3,c4,c5,c6);
#endif

	// Call ASM code for fast sieve
	fast_recip(vec,poff,curr_s,19);

	gettimeofday(&tv_sieve2, NULL);

	int sp0=0,sp1=0,sp2=0,sp3=0,sp4=0,sp5=0,sp6=0;

	mpz_t	bnTarget; mpz_init(bnTarget);
	//mpz_t	mpz_test; mpz_init(mpz_test);

#if CHECK
	// count and test
	uint32_t	offset = 0;
	mpz_t	tst; mpz_init(tst);

	for(int s=0;s<curr_s;s++) {
		for(uint32_t j=0;vec[s][j];j++) {
			//applog(LOG_INFO,"s:%u j:%u => %u",s,j,vec[s][j]);
			// Lets do a test
			//for(uint32_t p=0;p<pcount;p++) {
			for(uint32_t p=0;p<31;p++) {
				// Test
				uint32_t	r1,r2,r3,r4,r5,r6;

				mpz_add_ui(tst,b[s],vec[s][j]);
				r1 = mpz_fdiv_ui(tst,ps[p]);

				mpz_add_ui(tst,tst,4);
				r2 = mpz_fdiv_ui(tst,ps[p]);

				mpz_add_ui(tst,tst,2);
				r3 = mpz_fdiv_ui(tst,ps[p]);

				mpz_add_ui(tst,tst,4);
				r4 = mpz_fdiv_ui(tst,ps[p]);

				mpz_add_ui(tst,tst,2);
				r5 = mpz_fdiv_ui(tst,ps[p]);

				mpz_add_ui(tst,tst,4);
				r6 = mpz_fdiv_ui(tst,ps[p]);

				if((r1 == 0) || (r2 == 0) || (r3 == 0) || (r4 == 0) || (r5 == 0) || (r6 == 0))
				//if((r1*r2*r3*r4*r5*r6) == 0)
					applog(LOG_INFO,"32bit:%u p:%u poff[%u]:%u r1:%u r2:%u r3:%u r4:%u r5:%u r6:%u",vec[s][j],ps[p],s,poff[s][p],r1,r2,r3,r4,r5,r6);

			}
			offset++;
		}
	}
	applog(LOG_INFO,"%u left in %u streams",offset,curr_s);
	mpz_clear(tst);
	// end counter
#endif

	// We loop on the residuals
	struct mr_struct	mr;
	init_mymr(&mr,b[0]);	// b will be teh same size as n, so use that for sizing

	for(int s=0;s<curr_s;s++) {
		for(uint32_t j=0;vec[s][j];j++) {
			// Make the offset good
			mpz_add_ui( bnTarget, b[s], vec[s][j]);

			sp0++;
			// fermat tests are faster than the MR tests, so do them first
			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp1++; MPZ_FAST_ADD(bnTarget,4);

			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp2++; MPZ_FAST_ADD(bnTarget,2);

			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp3++; MPZ_FAST_ADD(bnTarget,4);

			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp4++; MPZ_FAST_ADD(bnTarget,2);

			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp5++; MPZ_FAST_ADD(bnTarget,4);

			if(__builtin_expect(!my_fermat(bnTarget, &mr),1) )
				goto final;
			sp6++;

			// Now the miller rabin tests
			// Rest the base for the MR tests
			mpz_add_ui( bnTarget, b[s], vec[s][j]);
			if(__builtin_expect(!my_millerrabin(bnTarget, MR_TESTS,&mr),1) )
				goto final;
			mpz_add_ui( bnTarget, bnTarget, 4);

			if(__builtin_expect(!my_millerrabin(bnTarget, MR_TESTS,&mr),1) )
				goto final;
			mpz_add_ui( bnTarget, bnTarget, 2);

			if(__builtin_expect(!my_millerrabin(bnTarget, MR_TESTS,&mr),1) )
				goto final;
			mpz_add_ui( bnTarget, bnTarget, 4);

			if(__builtin_expect(!my_millerrabin(bnTarget, MR_TESTS,&mr),1) )
				goto final;
			mpz_add_ui( bnTarget, bnTarget, 2);

			if(__builtin_expect(!my_millerrabin(bnTarget, MR_TESTS,&mr),1) )
				goto final;
			mpz_add_ui( bnTarget, bnTarget, 4);

			// this is a on success test
			if( my_millerrabin(bnTarget, MR_TESTS,&mr) || primes < 6 ) {
				applog(LOG_INFO,"thr:%u p6 found",thr_id);
				tp0 += sp0; tp1 += sp1; tp2 += sp2; tp3 += sp3; tp4 += sp4; tp5 += sp5; tp6 += sp6;
				//gmp_printf("P6   %#Zx\nb[%u] %#Zx\nn %" PRIx64 "\nvec %x\n",bnTarget,s,b[s],n[s],vec[s][j]);
				//gmp_printf("base %#Zx\n",base);
				*(uint64_t *)(pdata + RIECOIN_DATA_NONCE) = n[s] + vec[s][j];
				pdata[RIECOIN_DATA_NONCE] = swab32(pdata[RIECOIN_DATA_NONCE]);
				pdata[RIECOIN_DATA_NONCE+1] = swab32(pdata[RIECOIN_DATA_NONCE+1]);

				*hashes_done = max_nonce - n[s] + 1;
				mpz_clear(bnTarget);
				// Using 'i' as we can't use 's' in this loop
				for(int i=0;i<curr_s;i++) {
					free16(poff[i]); free16(vec[i]);
					mpz_clear(b[i]);
				}
				free_mymr(&mr);
				return 1;
			}
		final:	;
		}
	} //s
	
	// n is used to signal back, so need to "cap"
	
	tp0 += sp0; tp1 += sp1; tp2 += sp2; tp3 += sp3; tp4 += sp4; tp5 += sp5; tp6 += sp6;

	applog(LOG_INFO,"thread: %u TOTAL (p0,p1,p2,p3,p4,p5,p6) = (%u,%u,%u,%u,%u,%u,%u)",thr_id,tp0,tp1,tp2,tp3,tp4,tp5,tp6);
	sp0=sp1=sp2=sp3=sp4=sp5=sp6=0;

	//applog(LOG_INFO,"thread: %u n:%u first_nonce:%llu max_nonce:%llu r:%u",thr_id,n,first_nonce,max_nonce,r);
	*hashes_done = 0;	// Will sum up in the free memory loop
	n[0] += max_nonce;
	*(uint64_t *)(pdata + RIECOIN_DATA_NONCE) = n[3] + 0xfffffff0 - r[3];

	mpz_clear(bnTarget);
	for(int s=0;s<curr_s;s++) {
		*hashes_done += (0xfffffff0ULL - r[s]);
		free16(poff[s]); free16(vec[s]);
		mpz_clear(b[s]);
	}
	free_mymr(&mr);

	gettimeofday(&tv_end, NULL);
	timeval_subtract(&diff, &tv_offset, &tv_start);
	timeval_subtract(&diff1, &tv_sieve1, &tv_offset);
	timeval_subtract(&diff2, &tv_sieve2, &tv_sieve1);
	timeval_subtract(&diff3, &tv_end, &tv_sieve2);
	timeval_subtract(&diff4, &tv_end, &tv_start);
	applog(LOG_INFO,"thread: %u setup:%ld.%6.6lds sieve1:%ld.%6.6lds sieve2:%ld.%6.6lds fermat:%ld.%6.6lds total:%ld.%6.6lds",
	                thr_id,diff.tv_sec,diff.tv_usec, diff1.tv_sec,diff1.tv_usec, diff2.tv_sec,diff2.tv_usec, diff3.tv_sec,diff3.tv_usec,
	                diff4.tv_sec,diff4.tv_usec);

	// Adjust the global_s based on the time in diff4.tv_sec.  Only use thread 0's timing to avoid trashing and it
	// is also easier to syncronize over threads
	if(thr_id == 0) {
		// cheat and cap to 16 with a mask
		int	tmp;
		tmp = (int)((TARGET_T)/(diff4.tv_sec/(float)curr_s));

		// bound it
		if(tmp<1) global_s=1;
		else if(tmp>=MAX_S) global_s=MAX_S;
		else global_s=tmp;

		// report it
		if(global_s != curr_s) {
			applog(LOG_INFO,"adjusting streams from %u to %u",curr_s,global_s);
		}
	}
	return 0;
}
